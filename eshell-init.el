;;; This allows the command 'clear' to clear an eshell buffer.
(defun eshell/clear ()
  "Clear the eshell buffer."
  (let ((inhibit-read-only t))
    (erase-buffer)
        (eshell-send-input)))
