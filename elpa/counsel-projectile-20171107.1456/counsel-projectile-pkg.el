;;; -*- no-byte-compile: t -*-
(define-package "counsel-projectile" "20171107.1456" "Ivy integration for Projectile" '((counsel "0.8.0") (projectile "0.14.0")) :commit "5ec2118cbb3a6c047638ce305e50b1cd11ef3c03" :url "https://github.com/ericdanan/counsel-projectile" :keywords '("project" "convenience"))
