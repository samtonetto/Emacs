;;; -*- no-byte-compile: t -*-
(define-package "company-rtags" "20170924.2244" "RTags back-end for company" '((emacs "24.3") (company "0.8.1") (rtags "2.10")) :commit "1fe42080271cb42cf04b01d808d50bed9aef3d02" :url "http://rtags.net")
