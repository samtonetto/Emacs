(setq inhibit-startup-message t)
(tool-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

;;; allows you to save clipboard strings into emacs kill-ring
(setq save-interprogram-paste-before-kill t)

;; when a file changes in a buffer, automatically reload it.
;;(global-auto-revert-mode 1) ;; you might not want this
;;(setq auto-revert-verbose nil) ;; or this
(global-set-key (kbd "<f5>") 'revert-buffer)

;; let's you try packages
;; "M-x try" then enter a package name
(use-package try
  :ensure t)

;;; Display available key-bindings in popup
;;; C-x then wait a bit to see.
(use-package which-key
  :ensure t
  :config (which-key-mode))

(add-hook 'org-mode-hook
          (lambda ()
            (org-indent-mode t))
          t)

;;; Org-mode stuff
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;;; better C-x C-b buffer list
(defalias 'list-buffers 'ibuffer)
;;; (defalias 'list-buffers 'ibuffer-other-window)

;;; If you like a tabbar
;(use-package tabbar
;  :ensure t
;  :config
;  (tabbar-mode 1))

(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    ;;; this next bit customizes font
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

;; it looks like counsel is a requirement for swiper
(use-package counsel
  :ensure t
  :bind ("M-y" . counsel-yank-pop)
  )

(use-package swiper
  :ensure try
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> l") 'counsel-load-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    ))

;;; avy another way on navigating really quickly, based on ace-jump-mode from vim
;;; avy searches only on the screen, whereas swiper searches the whole doc
(use-package avy
  :ensure t
  :bind ("M-s" . avy-goto-char))

;; has a bunch of color themes
;; "M-x color-theme" then select one
(use-package color-theme
  :ensure t)

;; recommended by Mike Zamansky
(use-package zenburn-theme
  :ensure t
  :config (load-theme 'tango-dark t)
)

(use-package ox-reveal
:ensure ox-reveal)

(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(setq org-reveal-mathjax t)

(use-package htmlize
:ensure t)

(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    ))
;;;  (use-package company-quickhelp
;;;    :ensure t
;;;    :config
;;;    (global-company-mode)
;;;    (company-quickhelp-mode 1)
;;;    (setq company-quickhelp-delay 0.1))

;;;  (use-package flycheck
;;;    :ensure t
;;;    :init
;;;    (global-flycheck-mode t))

;;;  (use-package jedi
;;;    :ensure t
;;;    :init
;;;    (add-hook 'python-mode-hook 'jedi:setup)
;;;    (add-hook 'python-mode-hook 'jedi:ac-setup))
;;;  (use-package elpy
;;;    :ensure t
;;;    :config
;;;    (elpy-enable))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))

(use-package undo-tree
  :ensure t
  :init
  (global-undo-tree-mode 1))

;;; makes current line highlighted 
  (global-hl-line-mode t)
  ;;; beacon-mode: create
  (use-package beacon
    :ensure t
    :config
    (beacon-mode 1)
    ;;;(setq beacon-color ^#666)
    )

;; personally don't like hungry delete
;;  (use-package hungry-delete
;;    :ensure t
;;    :config
;;    (global-hungry-delete-mode))

  (use-package aggressive-indent
    :ensure t
    :config
    (global-aggressive-indent-mode 1))
  (use-package expand-region
    :ensure t
    :config
    (global-set-key (kbd "C-=") 'er/expand-region))

(use-package iedit
    :ensure t)

;;; begin dwim
(defun narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first. Narrowing to
org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if
         ;; you don't want it.
         (cond ((ignore-errors (org-edit-src-code) t)
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'latex-mode)
         (LaTeX-narrow-to-environment))
        (t (narrow-to-defun))))

;;(define-key endless/toggle-map "n"
;;  #'narrow-or-widen-dwim)

;; This line actually replaces Emacs' entire narrowing
;; keymap, that's how much I like this command. Only
;; copy it if that's what you want.
(define-key ctl-x-map "n" #'narrow-or-widen-dwim)
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (define-key LaTeX-mode-map "\C-xn"
              nil)))

(defun load-if-exists (f)
      "loads the elisp file only if it exists and is readable"
      (if (file-readable-p f)
	  (load-file f)))
	 
;;; e.g.
   ;; (load-if-exists "~/Dropbox/shared/mu4econfig.el")

(use-package web-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (setq web-mode-engines-alist
	'(("django" . "\\.html\\'")))
  (setq web-mode-ac-sources-alist
	'(("css" . (ac-source-css-property))
	  ("html" . (ac-source-words-in-buffer ac-source-abbrev))))
  (setq web-mode-enable-auto-closing t))

(set-face-attribute 'default nil :height 150)

(setq-default cursor-type 'box)
;;; other options: bar, hollow, hbar

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package vimish-fold
  :ensure t
  :config
  (progn
    (vimish-fold-global-mode 1)
    (global-set-key (kbd "C-M-x") #'vimish-fold)
    (global-set-key (kbd "C-M-d") #'vimish-fold-delete)
    (global-set-key (kbd "C-M-c") #'vimish-fold-toggle)
    (global-set-key (kbd "C-M-f") #'vimish-fold-toggle-all)
    ))

(setq c-default-style "bsd"
        c-basic-offset 4)

(use-package minimap)
(setq minimap-window-location 'right)

(use-package smooth-scroll
:ensure t
:config
  (smooth-scroll-mode 1)
  (setq smooth-scroll/vscroll-step-size 5)
  )

(global-unset-key (kbd "C-x C-c"))
(global-set-key (kbd "C-x C-c C-x C-c") 'save-buffers-kill-emacs)



(getenv "PATH")
  (setenv "PATH"
          (concat
           "/Library/TeX/texbin/" ":"
	   "/usr/local/bin" ":"
           "/usr/local/lib/pkgconfig" ":" 
           "/opt/X11/lib/pkgconfig" ":"
           (getenv "PATH")
	   )
)
(setq exec-path (append exec-path '("/usr/local/bin")))

(use-package pdf-tools
  :ensure t
  :config
  (custom-set-variables
    '(pdf-tools-handle-upgrades nil)) ; Use brew upgrade pdf-tools instead.
  (setq pdf-info-epdfinfo-program "/usr/local/bin/epdfinfo"))
(pdf-tools-install)

;; to use pdfview with auctex
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
   TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
   TeX-source-correlate-start-server t) ;; not sure if last line is neccessary

;; to have the buffer refresh after compilation
(add-hook 'TeX-after-compilation-finished-functions
       #'TeX-revert-document-buffer)

;;; Note: 'latex-mode-hook is emacs in-built latex, while
;;; 'LaTeX-mode-hook is AucTeX.
  (add-hook 'LaTeX-mode-hook 'turn-on-orgtbl)
  (add-hook 'LaTeX-mode-hook 'orgtbl-mode)

(use-package framemove
  :ensure t
  :config
  (windmove-default-keybindings)
  (setq framemove-hook-into-windmove t)
)

(use-package neotree
   :ensure t
   :config
   (global-set-key [f8] 'neotree-toggle)
   (setq neo-smart-open t)
   (setq projectile-switch-project-action 'neotree-projectile-action))

;;; "C-c p" to open projectile window
(use-package projectile
   :ensure t
   :config
   (projectile-global-mode)
   (setq projectile-completion-system 'ivy))
(use-package counsel-projectile
   :ensure t
   :config
   (counsel-projectile-on))

(use-package dumb-jump
  :bind (("M-g o" . dumb-jump-go-other-window)
         ("M-g j" . dumb-jump-go)
         ("M-g i" . dumb-jump-go-prompt)
         ("M-g x" . dumb-jump-go-prefer-external)
         ("M-g z" . dumb-jump-go-prefer-external-other-window))
  :config (setq dumb-jump-selector 'ivy) ;; (setq dumb-jump-selector 'helm)
  :ensure
  :init
  (dumb-jump-mode))

(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#F2804F")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))

(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
    (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
    (add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;;; (use-package indent-guide
  ;;; :ensure t
  ;;; :config (indent-guide-global-mode))

(add-to-list 'auto-mode-alist '("\\.tpp" . c++-mode))

(setq mouse-wheel-scroll-amount '(10))
;;;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
;;;  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 3) ;; keyboard scroll one line at a time

(global-set-key "\M-n" "\C-u1\C-v")
(global-set-key "\M-p" "\C-u1\M-v")
